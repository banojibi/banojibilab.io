---
title: "Sundarban Tour 2 Night 2 Days"
date: 2020-12-27T11:22:16+06:00
draft: false
description : "Sundarban Tour 2 Night 2 Days"
image: "images/blog/bonobibi.jpg"
categories:
- Packages
---

**The main attractions of the forest cottage**

1. As the BANOJIBI cottage is very close to the Sundarbans, it is very easy for the visitors to travel to the Sundarbans by staying in this cottage.
2. Opportunity to collect organic vegetables by hand from the vegetable garden of BANOJIBI Agro Farm
3. Opportunity for fishing from the pond with fishing hook and nets
4. Facilities to collect domestic eggs and eat meat from poultry farms
5. Facility to eat fresh sheep meat on the farm
6. Yogurt and confectionery made from the pure milk of the local cows
8. Opportunity to visit the location of Eco Village and various activities of Eco Village

**Facilities of the Cottages**

- Double bedroom for the couples (with modern bathroom facility)
- Facility to have 4 separate cottages (Banabibi, Bawal, Mouyal and Jele Cottage) for 4 couples at a time
- Opportunity to stay with the local people and get acquainted with their lifestyle
- Vacation in the natural environment of the Sundarbans
- Security measures by the local people

**Cost of staying in the forest cottage (per day)**

- Bangladeshi Guest 3,000 BDT
- Foreign Guest $ 60
- Need to pay 1000 BDT for tour Guide
