---
title: "Dried Shrimp"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Dried Shrimp"

# product Price
price: "300"
# priceBefore: "25.00"
amount: "250 gm"
# Product Short Description
shortDescription: "Dried Shrimp"

#product ID
productID: "7"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/shrimp.jpg"

---
