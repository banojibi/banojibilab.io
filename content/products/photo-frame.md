---
title: "Photo Frame"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Photo Frame"

# product Price
price: "400"
# priceBefore: "49.00"

amount: "1 Piece"
# Product Short Description
shortDescription: "Photo Frame"

#product ID
productID: "20"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/photo-frame.jpg"

---
