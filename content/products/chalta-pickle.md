---
title: "Chalta Pickle"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Chalta Pickle"

# product Price
price: "200"
#priceBefore: ""

amount: "250 gm"

# Product Short Description
shortDescription: "Chalta Pickle"

#product ID
productID: "3"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/chalta-pickle.jpg"

---
