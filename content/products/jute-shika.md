---
title: "Jute Shika"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Jute Shika"

# product Price
price: "300"
# priceBefore: "49.00"

amount: "1 Piece"
# Product Short Description
shortDescription: "Jute Shika"

#product ID
productID: "26"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/jute-shika.jpg"

---
