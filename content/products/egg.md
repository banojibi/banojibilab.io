---
title: "Fresh egg"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Fresh egg"

# product Price
price: "180"

amount: "12 pieces"
# Product Short Description
shortDescription: "Fresh egg"

#product ID
productID: "9"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/egg.JPG"

---
