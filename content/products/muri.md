---
title: "Puffed rice (Muri)"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Puffed rice (Muri)"

# product Price
price: "75"
# priceBefore: "49.00"

amount: "500 gm"
# Product Short Description
shortDescription: "Puffed rice (Muri)"

#product ID
productID: "14"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/muri.jpg"

---
