---
title: "Nipa Palm Hat"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Nipa Palm Hat"

# product Price
price: "400"
# priceBefore: "49.00"

amount: "1 Piece"
# Product Short Description
shortDescription: "Nipa Palm Hat"

#product ID
productID: "23"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/nipa-palm-hat.jpg"

---
