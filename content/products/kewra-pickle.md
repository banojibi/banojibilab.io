---
title: "Kewra Pickle"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Kewra Pickle"

# product Price
price: "200"
# priceBefore: "49.00"

amount: "250 gm"
# Product Short Description
shortDescription: "Kewra Pickle"

#product ID
productID: "17"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/kewra-pickle.jpg"

---
