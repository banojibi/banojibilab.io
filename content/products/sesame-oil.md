---
title: "Sesame oil"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Sesame oil"

# product Price
price: "350"
# priceBefore: "49.00"

amount: "250 gm"
# Product Short Description
shortDescription: "Sesame oil"

#product ID
productID: "16"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/sesame-oil.jpg"

---
